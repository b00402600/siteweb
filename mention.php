<section class="subpage-section section-legalnotices first">
    <div class="content">
        <h2>Mentions légales</h2>
        <p>Le site Internet <a href="https://www.PCrepair.fr/">www.PCrepair.fr</a> est édité par la société PCREPAIR, situé 76 RUE DU TROU GRILLON, 91280 SAINT-PIERRE-DU-PERRAY en FRANCE, dont le numéro SIRET est le : 841 178 791 00023.</p>
        <p>Pour contacter PCREPAIR :</p>
        <address>Par e-mail : <a href="mailto:contact@pcrepair.fr">contact@pcrepair.fr</a><br>Par courrier postal :<br>PCREPAIR<br>76 RUE DE L'AVENUE<br>91280 SAINT-PIERRE-DU-PERRAY<br>FRANCE</address>
        <h3>Responsable du site Web</h3>
        <p>Le directeur de la publication et du développement est Monsieur Gaëtan MAIURI &lt;<a href="mailto:maiuri.gaetan@PCrepair.fr">maiuri.gaetan@PCrepair.fr</a>&gt;.</p>
        <h3>Contact par voie électronique</h3>
        <p>Vous pouvez adresser par voie électronique vos demandes et réclamations, en vous adressant au service concerné :</p>
        <ul style="margin-top: 0px;">
            <li>Adresse générale, votre e-mail sera transféré dans le service approprié : <a href="mailto:contact@PCrepair.fr">contact@PCrepair.fr</a></li>
            <li>Address directe vers le service d'hébergement (serveurs, bases de données, etc.) : <a href="mailto:hostmaster@PCrepair.fr">hostmaster@PCrepair.fr</a></li>
            <li>Adresse directe vers le service de développement Web : <a href="mailto:webmaster@PCrepair.fr">webmaster@PCrepair.fr</a></li>
            <li>Adresse directe associée aux développeurs : <a href="mailto:developers@PCrepair.fr">developers@PCrepair.fr</a></li>
            <li>Adresse directe vers le service des relations clientelles : <a href="mailto:customer@PCrepair.fr">customer@PCrepair.fr</a></li>
            <li>Adresse directe vers le service d'aide : <a href="mailto:help@PCrepair.fr">help@PCrepair.fr</a></li>
        </ul>
        <h3>Hébergeur</h3>
        <p>Le site Internet <a href="https://www.PCrepair.fr/">www.PCrepair.fr</a> est hébergé par la société PCrepair.</p>
        <p>Toute réclamation devra être adressée à :</p>
        <address>PCrepair<br>SERVICE HEBERGEMENT<br>76 RUE DU TROU GRILLON<br>91280 SAINT-PIERRE-DU-PERRAY<br>FRANCE</address>
        <p>ou par e-mail en vous adressant directement à <a href="mailto:webmaster@PCrepair.fr">webmaster@PCrepair.fr</a>.</p>
        <h2>Autres informations</h2>
        <h3>Propriété intellectuelle</h3>
        <p>L’ensemble des éléments composant le site <a href="https://www.PCrepair.fr/">www.PCrepair.fr</a> tel que les textes, images, photographies, dessins, logos, marques, bases de données, vidéos, logiciels etc, sont la propriété exclusive de PCrepair ou du ou des tiers à qui ils appartiennent.</p>
        <p>L’utilisateur s’interdit d’y porter une atteinte quelconque.</p>
        <p>Toute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments est strictement interdite sans l'accord exprès par écrit de PCrepair.</p>
        <p>Cette représentation ou reproduction, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par les articles L.335-2 et suivants du Code de la propriété intellectuelle.</p>
        <p>Le non-respect de cette interdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur.</p>
        <p>En outre, les propriétaires des contenus copiés pourraient intenter une action en justice à votre encontre.</p>
        <h3>Hyperliens</h3>
        <p>Les utilisateurs et les visiteurs peuvent mettre un hyperlien sans autorisation préalable, en direction de la page d’accueil du site internet <a href="https://www.PCrepair.fr">www.PCrepair.fr</a>.</p>
        <h3>Informatiques et libertés</h3>
        <p>Les données recueillies sur le site sont destinées à l’usage de PCrepair à des fins de gestion administrative et ne font l’objet d’aucune communication à des tiers.</p>
        <p>Conformément à la loi informatique et libertés, l’utilisateur dispose d’un droit d’accès, de modification ou de suppression des données le concernant sur simple demande à l’adresse suivante: <a href="mailto:contact@PCrepair.fr">contact@PCrepair.fr</a>.</p>
        <p>Conformément à la loi 78-17 du 6 janvier 1978 (modifiée par la loi 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel) relative à l'informatique, aux fichiers et aux libertés, ce site a fait l'objet d'une déclaration sous le numéro 2036010 auprès de la Commission nationale de l'informatique et des libertés (<a href="https://www.cnil.fr/">www.cnil.fr</a>).</p>
        <h3>Responsabilité</h3>
        <p>Les informations contenues sur ce site sont aussi précises que possible, et le site est mis à jour à différentes périodes de l’année, mais peut toutefois contenir des inexactitudes ou des omissions.</p>
        <p>PCrepair ne saurait garantir l’exactitude, la complétude et l’actualité des informations diffusées sur son site.</p>
        <p>Les informations fournies le sont à titre indicatif.</p>
        <p>L’utilisateur reconnaît utiliser les informations sur le site sous sa responsabilité exclusive.</p>
        <h3>Loi applicable</h3>
        <p>Le présent site est soumis à la loi française.</p>
    </div>
</section>